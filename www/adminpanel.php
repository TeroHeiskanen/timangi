<?php
/**
 * Tulostaa taulun käyttäjistä ja inputit bannaamiseen/pisteiden poistamiseen.
 */
require_once 'sqlinit.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_SESSION['userId']) && in_array($_SESSION['type'], array("dev", "admin"))) {
    echo "
        <table class='adminpanelUserList'><thead><tr>\n";

    $fields = "userId,name,joinDate,loggedIn,active,type,status";

    foreach (explode(',', $fields) as $k => $h) {
        if ($k != "userId")
            echo "<th>$h</th>";
    }

    echo "<th>ban?</th>
        <th>delete scores</th>
        </tr></thead>\n<tbody>";

    $sql2 = "SELECT $fields FROM users";
    $stmt2 = $db->query($sql2, PDO::FETCH_ASSOC);

    foreach ($stmt2 as $row) {
        if ($row['userId'] == 13)
            continue; // skipataan system

        echo "<tr>";
        foreach ($row as $k => $field) {
            if ($k != "userId")
                echo "<td>$field</td>";
        }
        if (in_array($row['type'], array("admin", "dev"))) {
            echo "<td></td><td></td>";
        } else {
            if($row['status'] == "banned") $banChecked = " checked=checked ";
            else $banChecked = "";
            
            echo "<td><input class='adminpanelBanCheckbox' $banChecked type='checkbox' value='{$row['userId']}' name='ban[]' /></td>";
            echo "<td><input class='adminpanelDeleteScores' type='checkbox' value='{$row['userId']}' name='deleteScores[]' /></td>";
        }
        echo "</tr>\n";
    }

    echo "</tbody>\n</table>\n";
}
?>
