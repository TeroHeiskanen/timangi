<?php
/**
 * Bannaa valitut käyttäjät ja unbannaa muut.
 */
require_once 'sqlinit.php';

session_start();

if (!isset($_SESSION["userId"])) {
    exit;
}

if (!in_array($_SESSION['type'], array("admin", "dev"))) {
    exit;
}

if (!isset($_POST["ban"]) && !isset($_POST["deleteScores"])) {
    exit;
}

$banUsers = $_POST['ban'];
$deleteScores = $_POST['deleteScores'];

if (count($banUsers) > 0) {
    foreach ($banUsers as $userId) {
        $sql = "UPDATE users SET status='banned' WHERE userId=$userId";
        $db->exec($sql);
    }
    $sql = "UPDATE users SET status='' WHERE userId NOT IN (".join(",",$banUsers).")";
    $db->exec($sql);
}

if (count($deleteScores) > 0) {
    foreach ($deleteScores as $userId) {
        $sql = "DELETE scores.* FROM scores
        WHERE scores.userId = :userId";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':userId', $userId, PDO::PARAM_STR);
        $result = $stmt->execute();
    }
}
?>
