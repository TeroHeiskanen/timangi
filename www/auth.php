<?php
/**
 * Login, logout, register, settings ja adminpanel
 */
require_once "password.php"; // PHP 5.5 tyyliset password_ funktiot salasanojen kryptaamiseen (bcrypt)
require_once "sqlinit.php";  // yhteys MySQL tietokantaan PDO:lla

// login tiedon tarkastus
if (isset($_POST['loginnappi'])) {
    // Tarkistetaan tunnukset
    $str = "SELECT * FROM users WHERE name = :name";

    $stmt = $db->prepare($str);
    $stmt->bindValue(':name', $_POST['user'], PDO::PARAM_STR);
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);


    // tarkastetaan onko käyttäjä ollut aktiivisena viimeiseen tuntiin,
    // jos ei niin sallitaan uudelleen kirjautuminen vaikka ei ole logattu ulos
    $activityTime = strtotime($row['active']);
    $now = new DateTime("now");
    $now2 = strtotime($now->format("Y-m-d H:i:s"));
    $diff = $now2 - $activityTime;

    if ($row['loggedIn'] == 1 && $diff < 3600) {
        echo "<span class='loginerror'>Käyttäjä on jo kirjautunut.</span><br>\n";
    } else if ($row['status'] == "banned") {
        echo "<span class='loginerror'>Tunnus on bannattu.</span><br>\n";
    } else if (password_verify($_POST['salis'], $row['password'])) { // salasanan tarkastus hashista
        // onnistunut login
        $_SESSION['user'] = $row['name'];
        $_SESSION['userId'] = $row['userId'];

        $_SESSION['loggedIn'] = 1;
        $_SESSION['active'] = date("Y-m-d H:i:s");

        $_SESSION['type'] = $row['type'];
        $_SESSION['status'] = $row['status'];


        // päivitetään käyttäjän login tila ja aktiivisuus tietokantaan
        $str = "UPDATE users SET loggedIn=1, active='{$_SESSION['active']}' WHERE userId={$_SESSION['userId']}";
        $stmt = $db->prepare($str);
        $stmt->execute();

        $msg = $_SESSION['user'] . " kirjautui sisään.";
        $str2 = "INSERT INTO chatmessages (userId,msg,date,time) VALUES(13,:msg,:date,:time)";
        $stmt2 = $db->prepare($str2);

        $msg = stripslashes(htmlspecialchars($msg));
        $stmt2->execute(array(':msg' => $msg, ':date' => date("Y-m-d"), ':time' => date("H:i:s")));
    } else {
        echo "<span class='loginerror'>Väärät tunnukset.</span><br>\n";
    }
}

// rekisteröinnin prosesssointi
if (isset($_POST['registernappi'])) {
    $pattern = "/[^a-zA-Z0-9_]/";
    if(!preg_match($pattern, $_POST['reguser'])){
        try {
            // tarkastetaan onko valittu nimi jo käytössä
            $sql1 = "SELECT name FROM users WHERE name=:name";
            $stmt1 = $db->prepare($sql1);
            $stmt1->bindValue(':name', $_POST['reguser'], PDO::PARAM_STR);
            $stmt1->execute();

            if ($stmt1->rowCount() < 1) {
                $hash = password_hash($_POST['regsalis'], PASSWORD_DEFAULT); // salasanan hash

                $sql = "INSERT INTO users (name,password,joinDate) VALUES(:name, :password, :joindate)";
                $stmt = $db->prepare($sql);

                $stmt->execute(array(':name' => $_POST['reguser'], ':password' => $hash, ':joindate' => date("Y-m-d")));

                if ($stmt->rowCount() > 0) {
                    echo "Käyttäjä {$_POST['reguser']} rekisteröity.<br>\n";
                } else {
                    echo "<span class='loginerror'>nimi on jo rekisteröity</span><br>\n";
                }
            } else {
                echo "<span class='loginerror'>Nimi on jo käytössä.</span><br>\n";
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    } else {
        echo "<span class='loginerror'>Vääränlainen nimi, sallitut: 'a-z A-Z 0-9 _'</span><br>\n";
    }
}

// logout
if (isset($_REQUEST['action']) && $_REQUEST['action'] == "logout") {
    if (isset($_SESSION['userId'])) { // onko käyttäjä oikeasti kirjautuneena
        // merkitään tietokantaan logintila
        $str = "UPDATE users SET loggedIn=0 WHERE userId={$_SESSION['userId']}";
        $stmt = $db->prepare($str);
        $stmt->execute();

        $msg = $_SESSION['user'] . " kirjautui ulos.";
        $str2 = "INSERT INTO chatmessages (userId,msg,date,time) VALUES(13,:msg,:date,:time)";
        $stmt2 = $db->prepare($str2);

        $msg = stripslashes(htmlspecialchars($msg));
        $stmt2->execute(array(':msg' => $msg, ':date' => date("Y-m-d"), ':time' => date("H:i:s")));

        session_unset();
        session_destroy();
    }
}

//tunnuksen poistaminen, poistaa myös käyttäjän pisteet cascadella
if (isset($_POST['deleteaccount'])) {
    
    $sql = "DELETE FROM users WHERE userId = :userid";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':userid', $_SESSION['userId'], PDO::PARAM_INT);
    $result = $stmt->execute();
    
    if ($result) {
        echo "Käyttäjä {$_SESSION['user']} poistettu.<br>\n";

        session_unset();
        session_destroy();
    }
}

// pisteiden nollaus
if (isset($_POST['deletescores'])) {
    $sql = "DELETE scores.* FROM scores
            INNER JOIN users ON scores.userId = users.userId
            WHERE users.name = :name";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':name', $_SESSION['user'], PDO::PARAM_STR);
    $result = $stmt->execute();

    if ($result) {
        echo "Käyttäjän {$_SESSION['user']} pisteet nollattu.<br>\n";
    }
}

//tulostetaan login
if (!isset($_SESSION['user'])) {
    // käyttäjä ei ole logannut, tulostetaan login ja rekisteröinti formit
    echo "<span class='link' id='showlogin'>login</span>&nbsp;|&nbsp;<span class='link' id='showregister'>register</span>\n";
    echo "<div class='registerbg'>
            <div class='panel' id='panelLogin' style='display:none;'>
                <h1>Login:</h1>
                <form action='.{$_SERVER['PHP_SELF']}' method='post'>
                    <table class='login'>
                        <tr><th class='labelcol'></th><th class='fieldcol'></th></tr>
                        <tr><td>Nimi:</td><td><input type=text name='user'></td></tr>
                        <tr><td>Salasana:</td><td><input type=password name='salis'></td></tr>
                        <tr><td colspan='2'><input type=submit value='Login' name='loginnappi'></td></tr>
                    </table>
                </form>
            </div>
            <div class='panel' id='panelRegister' style='display:none;'>
                <h1>Rekisteröi:</h1>
                <form action='.{$_SERVER['PHP_SELF']}' method='post'>
                    <table class='register'>
                        <tr><th class='labelcol'></th><th class='fieldcol'></th></tr>
                        <tr><td>Nimi:</td><td><input type=text name='reguser'></td></tr>
                        <tr><td>Salasana:</td><td><input type=password name='regsalis'></td></tr>
                        <tr><td colspan='2'><input type=submit value='Rekisteröi' name='registernappi'></td></tr>
                    </table>
                </form>
            </div>
        </div>\n";
} else {
    // käyttäjä on kirjautunut, tulostetaan nimi ja asetus form
    echo "Kirjautunut käyttäjänä <span class='link' id='showsettings'>" . $_SESSION['user'] . "</span>. | ";
    if (in_array($_SESSION['type'], array( "admin", "dev" ))) {
        echo "<span class='link' id='showadminpanel'>admin</span> | ";
    }
    echo "<a href='.{$_SERVER['PHP_SELF']}?action=logout'>Logout</a>\n";

    echo "<div class='panel' id='panelSettings' style='display:none;'>
            <form action='.{$_SERVER['PHP_SELF']}' method='post'><input type='hidden' name='userId' value='{$_SESSION['userId']}'>
                <table class='settings'>
                    <tr><th class='labelcol'></th><th class='fieldcol'></th></tr>
                    <tr><td><input type='submit' value='Poista tunnus' name='deleteaccount'></td><td>Poistaa tunnuksen ja siihen liittyvät pisteet.</td></tr>
                    <tr><td><input type='submit' value='Nollaa pisteet' name='deletescores'></td><td>Nollaa pisteet.</td></tr>
                    
                </table>
            </form>
          </div>\n";
    if (in_array($_SESSION['type'], array( "dev","admin" ))) {
        echo "<div class='panel' id='panelAdmin' style='display:none'><form action='' id='adminpanelForm'><div id='adminpanelTable'>\n";
        include("adminpanel.php");
        echo "</div><input id='adminpanelSubmit' type='submit' value='Lähetä' name='adminpanelSubmit' />
        </form></div>\n";
    }
}
