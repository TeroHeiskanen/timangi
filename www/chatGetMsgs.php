<?php
/***
 * Hakee chat viestit tietokannasta ja tulostaa ne. Max 50 viimeisintä viestiä.
 */

require_once 'sqlinit.php';
$lastMsgId = -1;

if(isset($_REQUEST['lastMsgId'])){
    $lastMsgId = intval($_REQUEST['lastMsgId']);
}

$sql = "SELECT * FROM (SELECT c.*, u.name FROM chatmessages AS c
        INNER JOIN users AS u ON u.userId = c.userId 
        WHERE msgId > :lastMsgId
        ORDER BY c.msgId DESC LIMIT 50) sub ORDER BY msgId ASC";
$stmt = $db->prepare($sql);
$stmt->bindValue(':lastMsgId', $lastMsgId, PDO::PARAM_INT);

$stmt->execute();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    if($row['name'] != "system"){
        //normaalit viestit
        echo "<div class='msgln'><span id='msgid'>".$row['msgId']."</span>(".$row['time'].") <b>".$row['name']."</b>: ".$row['msg']."</div>";
    } else {
        //tulostettaan system viestit
        echo "<div class='msglnsystem'><span id='msgid'>".$row['msgId']."</span>(".$row['time'].") " .$row['msg']."</div>";
    }
}

?>
