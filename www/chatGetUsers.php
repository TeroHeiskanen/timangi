<?php
/**
 * Hakee chatin aktiiviset käyttäjät tietokannasta.
 * Lisäksi päivittää käyttäjän aktiivisuuden ja hallitsee bannit.
 */
require_once 'sqlinit.php';
session_start();

$now = time();
$sql = "SELECT name, type, userId, status, active FROM users
        WHERE active > :active
        AND loggedIn = 1";
$stmt = $db->prepare($sql);
$stmt->bindValue("active", date("Y-m-d H:i:s", $now - 11 * 60));
$stmt->execute();

// listataan online käyttäjät
foreach($stmt as $row){
    $inactiveFor = ($now - strtotime($row["active"])) / 60;

    if($inactiveFor<10){
        echo "<div class='chatUser'>".$row['name'];
        if($row['type']){
            echo " <span class='usertype'>&lt;".$row['type']."&gt;</span>";
        }
        if($inactiveFor > 1){
            $afktimer = intval($inactiveFor + 1) - 1;
            echo " <span class='afktimer'>(".$afktimer."min)</span>";
        }
        echo "</div>";
    } else { // listattava käyttäjä ollut yli 10 minuuttia idlenä (=sulkenut selaimen kirjautumatta ulos)
        $str = "UPDATE users SET loggedIn=0 WHERE userId={$row['userId']}";
        $stmt2 = $db->prepare($str);
        $stmt2->execute();
        
        $msg = $row['name']. " kirjattiin ulos epäaktiivisuuden vuoksi.";
        $str2 = "INSERT INTO chatmessages (userId,msg,date,time) VALUES(13,:msg,:date,:time)";
        $stmt = $db->prepare($str2);
        $msg = stripslashes(htmlspecialchars($msg));
        $stmt->execute(array(':msg' => $msg, ':date' => date("Y-m-d"), ':time' => date("H:i:s")));  
    }    
}


if(isset($_SESSION['userId'])){
    $str = "SELECT * FROM users WHERE userId = :userid";

    $stmt = $db->prepare($str);
    $stmt->bindValue(':userid', $_SESSION['userId'], PDO::PARAM_INT);
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    // tarkistetaan käyttäjän tila ( ban systeemi v1 )
    if ($row['userId'] == $_SESSION['userId'] && $row['status'] == "banned") {
        // merkitään tietokantaan logintila
        $str = "UPDATE users SET loggedIn=0 WHERE userId={$_SESSION['userId']}";
        $stmt = $db->prepare($str);
        $stmt->execute();

        $msg = $_SESSION['user'] . " BANNATTIIN.";
        $str2 = "INSERT INTO chatmessages (userId,msg,date,time) VALUES(13,:msg,:date,:time)";
        $stmt2 = $db->prepare($str2);

        $msg = stripslashes(htmlspecialchars($msg));
        $stmt2->execute(array(':msg' => $msg, ':date' => date("Y-m-d"), ':time' => date("H:i:s")));

        session_unset();
        session_destroy();
        echo "<input type='hidden' id='forcerefresh' class='forcerefresh' style='display:none;' value='true'/>"; // pakotetaan uudelleen lataus clientissä
    } else { // päivitetään tässä kohtaa käyttäjän aktiivisuus
        $active = date("Y-m-d H:i:s");
        $str = "UPDATE users SET active='$active' WHERE userId={$_SESSION['userId']}";
        $stmt2 = $db->prepare($str);
        $stmt2->execute();

        $_SESSION['active'] = $active;
    }
}
?>
