<?php

/**
 * Postaa chat viestin tietokantaan jos käyttäjä on kirjautunut ja ei ole bannattu.
 */
require_once 'sqlinit.php';
session_start();

if (!isset($_SESSION["user"])) {
	exit;
}

if (isset($_SESSION["status"]) && $_SESSION['status'] == "banned") {
	exit;
}

if (!isset($_POST["text"])) {
	exit;
}


$text = $_POST["text"];

if (strlen($text) > 2) {
	$text = substr($text, 0, 2)."...";
}

$sql = "INSERT INTO chatmessages (userId,msg,date,time,ip) VALUES(:userId,:msg,:date,:time,:ip)";
$stmt = $db->prepare($sql);
$msg = stripslashes(htmlspecialchars($text));
$stmt->execute(array(':userId' => $_SESSION['userId'], ':msg' => $msg, ':date' => date("Y-m-d"), ':time' => date("H:i:s"), ':ip' => $_SERVER['REMOTE_ADDR']));

?>
