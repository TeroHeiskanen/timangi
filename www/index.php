<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Timangi</title>

        <link href="assets/demoStyles.css" rel="stylesheet" type="text/css" />
        <!-- jquery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <script type="text/javascript" src="scripts/sitecontrols.js"></script>

        <!-- CreateJS classes here: -->
        <script type="text/javascript" src="scripts/createjs-2013.12.12.min.js"></script>

        <!-- The game itself: -->
        <script type="text/javascript" src="scripts/timangi.js"></script>
            
    </head>

    <body onload="init();">
        <table class="site"><tr><td>
<!-- ######################### header alkaa ###################################-->
        <div id="header">
            <h1><a href="index.php"><span>Timangi webbipeli</span></a></h1>
            <p>Tervetuloa pelaamaan Timangia.</p>
            <?php
            include("auth.php");
            ?>
        </div>
<!-- header loppuu -->
        <div class="maincontainer">
            <table class="table">
                <tr>
                    <td class="leftContainer">
<!-- ######################### scorelist alkaa ###################################-->
                        <div class="leftContainer">
                            <?php
                            include("scores.php");
                            ?>
                        </div>
<!-- score list loppuu -->
                    </td>
                    <td class="centerContainer">
<!-- ######################### keskiosio alkaa ###################################-->
                        <div class="canvasHolder">
                            <canvas id="gameCanvas" width="600" height="600"></canvas>
                        </div>
                        
                        <audio id="background_audio" muted loop autoplay>
                            <source src="assets/music.mp3" type="audio/mpeg">
                        </audio>
                        
                        <div id="soundboard">
                            <button onclick="document.getElementById('background_audio').muted=!document.getElementById('background_audio').muted;">
                                <img src="assets/mute.png" alt="" />
                            </button>
                            <button onclick="document.getElementById('background_audio').volume+=0.1;">
                                <img src="assets/volumeUP.png" alt=""  />
                            </button>
                            <button onclick="document.getElementById('background_audio').volume-=0.1;">
                                <img src="assets/volumeDOWN.png" alt="" />
                            </button>
                        </div> 
<!-- keskiosio loppuu -->
                    </td>
                    <td class="rightContainer">
<!-- ######################### chatosio alkaa ###################################-->
                        <div class="rightContainer">
                            <?php
                            include("chat.php");
                            ?>
                        </div>
<!-- chatosio loppuu -->
                    </td>
                </tr>
            </table>
        </div>
        </td></tr></table>
    </body>
</html>
