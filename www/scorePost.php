<?php
/**
 * Postaa käyttäjän scoren tietokantaan.
 * HUOM tarvitaan vielä tarkastus scoren aitoudelle!
 */
require_once 'sqlinit.php';

session_start();

if(isset($_SESSION['user']) && $_SESSION['status'] != "banned" && isset($_POST["score"])){
    $score = $_POST['score'];
    
    $sql = "INSERT INTO scores (userId,score,date) VALUES(:userId,:score,:date)";
    $stmt = $db->prepare($sql);
    
    $stmt->execute(array(':userId' => $_SESSION['userId'], ':score' => $score, ':date' => date("Y-m-d H:i:s")));
    
    $msg = $_SESSION['user']. " sai pisteet $score.";
    $str = "INSERT INTO chatmessages (userId,msg,date,time) VALUES(13,:msg,:date,:time)";
    $stmt = $db->prepare($str);
    
    $msg = stripslashes(htmlspecialchars($msg));
    $stmt->execute(array(':msg' => $msg, ':date' => date("Y-m-d"), ':time' => date("H:i:s")));
}
?>