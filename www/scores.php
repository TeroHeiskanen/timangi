<?php
/**
 * Tulostaa scorelistan ja kontrollit filtteröintiin.
 */

echo "<h1>Pisteet:</h1>\n";
echo "<span id='scoreFilterAll' class='link'>Kaikki</span> | <span id='scoreFilterToday' class='link'>Tänään</span>";
if(isset($_SESSION['userId'])) 
        echo " | <span id='scoreFilterMe' class='link'>Omat</span>";
echo "\n<div class='scores'>\n";
include("scoresList.php");
echo "</div>\n";
?>