<?php
/**
 * Tulostaa scorelistan valitulla filtterillä
 */
require_once 'sqlinit.php';

echo "<table class='scorestable'>\n";

$sql = "SELECT u.name, s.score, s.date FROM scores AS s
        INNER JOIN users AS u ON u.userId = s.userId ";

$scoreFilter = isset($_GET["scoreFilter"]) ? $_GET["scoreFilter"] : null;
$params = [];

if($scoreFilter == "today"){
    $sql.= "WHERE DATE(s.date) = DATE(NOW()) ";
}
if($scoreFilter == "me" && isset($_GET["userName"])){
    $sql.= "WHERE u.name LIKE :username ";
    $params["username"] = $_GET["userName"];
}

$sql.= "ORDER BY s.score DESC
        LIMIT 30";

$stmt = $db->prepare($sql);

foreach($params as $param => $value) {
	$stmt->bindValue(":".$param, $value, PDO::PARAM_STR);
}

$stmt->execute();

while($row = $stmt->fetch()){
    echo "<tr><td>".$row['name']."</td><td>".$row['score']."</td></tr>\n";
}
        
echo "</table>\n";

?>
