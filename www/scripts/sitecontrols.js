/***
 * Tiedosto sisältää chatin toimintoja ja sivun yleisiä jquery toimintoja.
 */

// globaali muuttuja chatin viimeisen viestin id:lle
window.lastMsgId = -1;
window.oldLastMsgId = -1;
window.chatMute = 0;

var window_focus = true;

window.onblur = function() {
    window_focus = false;
};
window.onfocus = function() {
    window_focus = true;
};

/**
 * Hakee viestit tietokannasta. Jos window.lastMsgId on muuta kuin -1, haetaan
 * vain uudet viestit.
 */
function loadMsgs() {
    var oldscrollHeight = $(".chatmessages").prop("scrollHeight"); //Scroll height before the request
    var forceRefresh = $('input#forcerefresh').val();
    if(forceRefresh === "true"){
        location.reload();
    }
    
    $.ajax({
        url: "chatGetMsgs.php?lastMsgId=" + window.lastMsgId,
        cache: false,
        success: function(html) {
            $(".chatmessages").html($(".chatmessages").html() + html);

            //Auto-scroll          
            var newscrollHeight = $(".chatmessages").prop("scrollHeight");
            if (newscrollHeight > oldscrollHeight) {
                $(".chatmessages").animate({scrollTop: newscrollHeight}, "slow"); //Autoscroll to bottom of div
            }
            // haun jälkeen asetetaan viimeisen viestin id
            window.lastMsgId = $("span#msgid:last").html();

            // viestiääni jos ei ole focusta ikkunassa
            if (window.oldLastMsgId !== -1 && window.oldLastMsgId !== window.lastMsgId && window_focus === false) {
                var audioElement = $("#beep")[0];
                audioElement.play();
            }
            window.oldLastMsgId = window.lastMsgId;

            //testaus viesti
            $(".testmessages").html("Bytes loaded:" + html.length);
        }
    });
}

/**
 * Hakee käyttäjälistan tietokannasta.
 */
function loadUsers() {
    $.ajax({
        url: "chatGetUsers.php",
        cache: false,
        success: function(html) {
            $(".usercontainer").html(html);
        }
    });
}

/**
 * Vaihtaa chatin mute tilaa
 */
function chatToggleMute() {
    if (window.chatMute === 1) {
        $("#beep")[0].volume = 0.5;
        window.chatMute = 0;
        $("#mute").prop("src", "assets/normal.png");
    } else {
        $("#beep")[0].volume = 0;
        window.chatMute = 1;
        $("#mute").prop("src", "assets/mute.png");
    }
}

/**
 * Päivittää score listan 
 */
function updateScores() {
    $("div.leftContainer").load("scores.php");
}

/**
 * Click handlerit ja sivun latauksen jälkeen suoritettavat.
 */
$(document).ready(function() {
    /******  header stuffia **********/
    // rekisteröinti divin handler
    $("#showregister").click(function() {
        $("div.panel").hide();
        $("div#panelRegister").toggle();
    });

    // login divin handler
    $("#showlogin").click(function() {
        $("div.panel").hide();
        $("div#panelLogin").toggle();
    });

    // settings divin handler
    $("#showsettings").click(function() {
        $("div.panel").hide();
        $("div#panelSettings").toggle();
    });
    
    // adminpanel 
    $("#showadminpanel").click(function() {
        $("div.panel").hide();
        $("div#panelAdmin").toggle();
    });
    
    // handler: suljetaan paneelit jos käyttäjä klikkaa pääsivulle
    $(".maincontainer").click(function() {
        $("div.panel").hide();
    });



    /************ chatin stuffia ***********/
    // tekstikentän submit handler, postataan viesti php skriptalle chatPost.php
    $("#submitmsg").click(function() {
        var clientmsg = $("#msg").val();
        $("#msg").val("");
        $.post("chatPost.php", {text: clientmsg});
        return false;
    });

    // vieritetään chatmessages div loppuun sivun latauksen yhteydessä
    $(".chatmessages").animate({scrollTop: $(".chatmessages").prop("scrollHeight")}, "fast");

    var audioElement = $("#beep")[0];
    audioElement.volume = 0.5;

    // chatin mute nappi
    $(".chatcontrols").click(function() {
        chatToggleMute();
    });


    // haetaan käyttäjälista ja kaikkiviestit
    loadUsers();
    loadMsgs();

    // asetetaan timerit viestien ja käyttäjien päivittämiselle
    setInterval(loadMsgs, 1000);
    setInterval(loadUsers, 10000);


    /***************** score juttuja ***********/

    $("#scoreFilterToday").click(function() {
        $("div.scores").load("scoresList.php?scoreFilter=today");
    });

    $("#scoreFilterMe").click(function() {
        var user = $("#showsettings").html();
        $("div.scores").load("scoresList.php?scoreFilter=me&userName=" + user);
    });

    $("#scoreFilterAll").click(function() {
        $("div.scores").load("scoresList.php");
    });
    
    
    
    /*************** admin panel ****************/
    $("#adminpanelSubmit").click(function(){
        $.post("adminpanelBan.php", $( "#adminpanelForm" ).serialize());
        
        setTimeout(() => {$("#adminpanelTable").load("adminpanel.php")}, 1000);
        return false;
    });
});
