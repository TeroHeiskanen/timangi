	//GLOBAALIT MUUTTUJAT
    var loader, w, h, stage, points, timer;
    
	var startTime;
	var timeLeft;
	
	var currentTileset;
	
    var timerStart = false;
	var update = true;
    var queueChanged = false;	
	var mouseEnabled = true;
	var scorePosted = false;
	
	var inMenu;
	var inGame;
	var inGameOver;
    
    var updateQueue = new Array();	
	
    var selection = null;
	var dragStartX;
	var dragStartY;
    
	var gemMatrix;
	var imageMatrix;
	var flyingGems;
	
	//VAKIOMUUTTUJAT
	
    var IMG_SIZE = 65;
    var MATRIX_SIZE = 8;
	var START_TIME = 100;
    
	//OLIOT
	
	function GemVector(x,y, gemType)
	{
		this.x = x;
		this.y = y;
		this.gemType = gemType;
	}
	
	function GemObject() 
	{
		this.gemType = "gemEmpty";
		this.gemX = 0;
		this.gemY = 0;	
		this.cursor = "pointer";
		this.deleteThis = false;		
	}
	
	function FlyingGemObject(x, y, speed, image)
	{
		this.image = image;
		this.speed = speed;
		this.x = x;
		this.y = y;		
	}
	
	FlyingGemObject.prototype = new createjs.Bitmap();
	
	function ImageObject()
	{	
		this.posX = 0;
		this.posY = 0;
		
		var hit = new createjs.Shape();
		hit.graphics.beginFill("#000").drawRect(0,0,65,65);
		this.hitArea = hit;
		
		this.on("mouseover", function(evt)
		{
			this.scaleX = 1.1;
			this.scaleY = 1.1;
			this.x = this.x - 3;
			this.y = this.y - 2;
			update = true;
		});

		this.on("mouseout", function(evt)
		{
			this.scaleX = 1;
			this.scaleY = 1;
			this.x = this.x + 3;
			this.y = this.y + 2;				
			update = true;
		});
		
		this.on("mousedown", function(evt)
		{
			if(mouseEnabled)
			{
				timerStart = true;
				dragStartX = evt.stageX;
				dragStartY = evt.stageY;
			}
		});
		
		this.on("pressup", function(evt)
		{		
			if(mouseEnabled)
			{
				var firstGem, secondGem;
				
				if(Math.abs(dragStartX - evt.stageX) > Math.abs(dragStartY - evt.stageY))
				{
					if(dragStartX > evt.stageX && this.posX > 0)
					{
						firstGem = gemMatrix[this.posX][this.posY];
						secondGem = gemMatrix[this.posX-1][this.posY];
					}
					else if(this.posX < (MATRIX_SIZE-1))
					{
						firstGem = gemMatrix[this.posX][this.posY];
						secondGem = gemMatrix[this.posX+1][this.posY];
					}
				}
				else
				{
					if(dragStartY > evt.stageY && this.posY > 0)
					{
						firstGem = gemMatrix[this.posX][this.posY];
						secondGem = gemMatrix[this.posX][this.posY-1];
					}
					else if(this.posY < (MATRIX_SIZE-1))
					{
						firstGem = gemMatrix[this.posX][this.posY];
						secondGem = gemMatrix[this.posX][this.posY+1];
					}
				}
				
				if(firstGem != null && secondGem != null)
				{
					if(swapGems(firstGem.gemX,firstGem.gemY,secondGem.gemX,secondGem.gemY));
					{
						do
						{
							currentLength = updateQueue.length;
							for(var i = 0 ; i < MATRIX_SIZE ; i++)
							{
								for(var j = 0 ; j < MATRIX_SIZE ; j++)
								{
									if(!gemMatrix[i][j].deleteThis)
									{
										checkChain(i,j,gemMatrix[i][j].gemType,"",0);
									}
								}
							}
							checkDrops();							
						}while(currentLength != updateQueue.length);

						if(updateQueue.length === 0)
						{
							swapGems(firstGem.gemX,firstGem.gemY,secondGem.gemX,secondGem.gemY);
						}
						else
						{					
							queueChanged = true;
						}
					}
				}
			}
		});
	}
		
	ImageObject.prototype = new createjs.Bitmap();
	
	function ButtonElement(text, font, color, onClick)
	{
		this.text = text;
		this.actionOnClick = onClick;
		this.font = font;
		this.color = color;
		this.height = this.getMeasuredHeight();
		this.width = this.getMeasuredWidth();
		this.regX = this.width/2;
		this.regY = this.height/2;
		
		var hit = new createjs.Shape();
		hit.graphics.beginFill("#000").drawRect(0,0,this.width,this.height);
		this.hitArea = hit;
		
		this.on("click",this.actionOnClick);
		
		this.on("mouseover", function(evt)
		{
			this.shadow = new createjs.Shadow("#000000", 5, 5, 5);
			update = true;
		});
		
		this.on("mouseout", function(evt)
		{
			this.shadow = null;
			update = true;
		});
	}
	
	ButtonElement.prototype = new createjs.Text();
	
	//DEBUG PÄIVITYS
	
	function refresh(evt)
	{
		for(var i = 0 ; i < MATRIX_SIZE ; i++)
		{
			for(var j = 0 ; j < MATRIX_SIZE ; j++)
			{
				updateImageAt(i,j,gemMatrix[i][j].gemType);
			}
		}
	}
	
	//INIT-FUNKTIOT
	
    function init() {
        var canvas = document.getElementById("gameCanvas");
        stage = new createjs.Stage(canvas);
		loader = new createjs.LoadQueue(false);	
		
        createjs.Touch.enable(stage);
        stage.enableMouseOver(100);
        stage.mouseMoveOutside = true;       
        
		w = stage.canvas.width;
        h = stage.canvas.height;
		
		createjs.Ticker.setFPS(60);
        createjs.Ticker.addEventListener("tick", tick);		
		createjs.Sound.registerSound({id:"blop", src:"assets/blop.mp3"});
		
		currentTileset = "diamonds";
		
		initMenu();
    }
    
	//VALIKKONÄKYMÄN LUONTI
	
	function initMenu()
	{
		stage.removeAllChildren();
		
		inMenu = true;
		inGame = false;
		inGameOver = false;
		
		var newGameButton = new ButtonElement("NEW GAME", "24px Chiller", "#e7841d", initNewGame);
		var textChangeTileset = new createjs.Text("TILESETS:", "24px Chiller", "#e7841d");
		var gemTileset = new ButtonElement("GEMS", "24px Chiller", "#e7841d", function(evt){currentTileset = "diamonds"; initMenu();});
		var corgiTileset = new ButtonElement("CORGIS", "24px Chiller", "#e7841d", function(evt){currentTileset = "corgis"; initMenu();});
		var runeTileset = new ButtonElement("RUNES", "24px Chiller", "#e7841d", function(evt){currentTileset = "runes"; initMenu();});	
		
		switch(currentTileset)
		{
			case "diamonds":
				gemTileset.color = "#e20101";
				break;
			case "corgis":
				corgiTileset.color = "#e20101";
				break;
			case "runes":
				runeTileset.color = "#e20101";
				break;
		}
		
		background = new createjs.Bitmap();
		
		newGameButton.x = (w/2);
		newGameButton.y = 200;
		
		textChangeTileset.regX = textChangeTileset.getMeasuredWidth()/2;
		textChangeTileset.x = (w/2);
		textChangeTileset.y = 250;
		
		gemTileset.x = (w/2);
		gemTileset.y = 300;
		
		corgiTileset.x = (w/2);
		corgiTileset.y = 330;
		
		runeTileset.x = (w/2);
		runeTileset.y = 360;
		
		stage.addChild(background);
		stage.addChild(newGameButton);
		stage.addChild(textChangeTileset);
		stage.addChild(gemTileset);
		stage.addChild(corgiTileset);
		stage.addChild(runeTileset);
		
		initMenuImages();
	}
	
	//PELINÄKYMÄN LUONTI
	
	function initNewGame()
	{
		stage.removeAllChildren();
		
		inMenu = false;
		inGame = true;
		inGameOver = false;
		
		noMovesLeft = false;
		
		mouseEnabled = true;
		scorePosted = false;
		
		points = new createjs.Text(0, "32px Chiller", "#e7841d");
        timer = new createjs.Text(START_TIME, "32px Chiller", "#e7841d");
		var buttonToMenu = new ButtonElement("BACK TO MENU", "18px Chiller", "#e7841d", initMenu);		
		
		buttonToMenu.regX = 0;
		buttonToMenu.regX = 0;
		buttonToMenu.x = 40;
		buttonToMenu.y = h-10;
				
		background = new createjs.Bitmap();
				
		background.x = 0;
		background.y = 0;
		points.x = 110;
        points.y = 0;
        timer.x = 500;
        timer.y = 0;  
		
		stage.addChild(background);
        stage.addChild(points);
        stage.addChild(timer);
		stage.addChild(buttonToMenu);
		
		timeLeft = 100;
		startTime = 0;
		
		initGameBoard();
		initGameImages(currentTileset);
	}
	
	//GAMEOVER-NÄKYMÄN LUONTI
	
	function initGameOver(message)
	{
		stage.removeAllChildren();
		
		inMenu = false;
		inGame = false;
		inGameOver = true;	
		
		var textGameOver = new createjs.Text(message, "32px Chiller", "#e7841d");
		var textEndScore = new createjs.Text("YOU GOT " + points.text + " POINTS!", "32px Chiller", "#e7841d");
		var buttonToMenu = new ButtonElement("BACK TO MENU", "18px Chiller", "#e7841d", initMenu);	
		
                
                
		textGameOver.regX = textGameOver.getMeasuredWidth()/2;
		textGameOver.x = w/2;
		textGameOver.y = (h/2);
                textGameOver.shadow = new createjs.Shadow("#000000", 5, 5, 5);
		
		textEndScore.regX = textEndScore.getMeasuredWidth()/2;
		textEndScore.x = w/2;
		textEndScore.y = (h/2)+50;
                textEndScore.shadow = new createjs.Shadow("#000000", 5, 5, 5);
		
		buttonToMenu.regY = 0;
		buttonToMenu.x = w/2;
		buttonToMenu.y = (h/2)+160;
                buttonToMenu.shadow = new createjs.Shadow("#000000", 5, 5, 5);

		initFlyingGems();
		
		stage.addChild(textGameOver, textEndScore, buttonToMenu);	
		
		update = true;
	}
	
	//PELIKENTÄN ALUSTUS
	
	function initGameBoard()
	{
		gemMatrix = new Array(MATRIX_SIZE);
        imageMatrix = new Array(MATRIX_SIZE);
		
		for (var i = 0; i < MATRIX_SIZE; i++)
		{
			gemMatrix[i] = new Array(MATRIX_SIZE);
			imageMatrix[i] = new Array(MATRIX_SIZE);
			
			for (var j = 0; j < MATRIX_SIZE; j++)
			{		
				var gem = new GemObject();
				var img = new ImageObject();
				
				gem.gemX = i;
				gem.gemY = j;
				gem.gemType = "gemEmpty";
				gemMatrix[i][j] = gem;
				
				img.x = (i*65)+40;
				img.y = (j*65)+40;
				img.posX = i;
				img.posY = j;
				stage.addChild(img);		
				imageMatrix[i][j] = img;
			}
		}	
	}
	
	//VALIKON KUVIEN LATAUS
	
	function initMenuImages()
	{
		loader.removeAll();
		loader.removeAllEventListeners("complete");
		loader.on("complete", menuLoadingComplete);
		loader.loadFile({src:"assets/menu.png", id:"background"});
	}
	
	//PELIN KUVIEN LATAUS
	
	function initGameImages(tileSet)
	{
		loader.removeAll();
		loader.removeAllEventListeners("complete");
		loader.on("complete", gameLoadingComplete);		
	
		var tileSetFolder = "assets/tilesets/";
		
		switch(tileSet)
		{
			case "runes":
				tileSetFolder += "runes/";
				break;
			case "diamonds":
				tileSetFolder += "diamonds/";
				break;
			case "corgis":
				tileSetFolder += "corgis/";
				break;
			default:
				tileSetFolder += "runes/";
				break;
		}
		
		manifest = 
		[			
			{src:"assets/alphamaskfilter.png", id:"alphamask"},
			{src:"assets/tausta.png", id:"background"}
		];		
		
		gems = 
		[			
			{src:tileSetFolder+"gemBlue.png", id:"gemBlue"},
            {src:tileSetFolder+"gemYellow.png", id:"gemYellow"},
            {src:tileSetFolder+"gemOrange.png", id:"gemOrange"},
            {src:tileSetFolder+"gemGreen.png", id:"gemGreen"},
            {src:tileSetFolder+"gemWhite.png", id:"gemWhite"},
            {src:tileSetFolder+"gemRed.png", id:"gemRed"},
            {src:tileSetFolder+"gemPurple.png", id:"gemPurple"}
		];		
				
		loader.loadManifest(manifest);
		loader.loadManifest(gems);		
	}		
	
	//GAMEOVER-NÄKYMÄN TAUSTAN LUONTI
	
	function initFlyingGems()
    {
		flyingGems = new Array();
		for(var i = 0 ; i < (Math.random() * 5) + 25 ; i++)
		{
			flyingGems.push(new FlyingGemObject(Math.random()*w,(Math.random()*h)-65,(Math.random()*5)+5,loader.getResult(randomGem())));
			stage.addChild(flyingGems[i]);
		}
    }
	
	//AJETAAN KUN VALIKON KUVAT ON LADATTU
	
	function menuLoadingComplete()
	{
		background.image = loader.getResult("background");
		update = true;
	}
	
	//AJETAAN KUN PELIN KUVAT ON LADATTU
	
    function gameLoadingComplete()
    {        
        background.image = loader.getResult("background");
        fillMatrix();	
		update = true;
    }
   
	//PELIKENTÄN PÄIVITYSTÄ
   
    function tick(event) {
		if(inGame)
		{
			if(queueChanged)
			{
				handleUpdateQueue();
			}
			
			if(timerStart)
			{
				if(startTime === 0)
				{
					startTime = new Date().getTime();
				}
				
				var time = new Date().getTime();
				timeLeft = START_TIME-Math.floor((time - startTime)/1000);
				
				if(timer.text != timeLeft)
				{
					timer.text = timeLeft;
					update = true;
				}			
				
				if(timeLeft <= 0)
				{
					timerStart = false;
					mouseEnabled = false;							
				}
			}	
			
			if(timeLeft <= 0 && updateQueue.length === 0 && !scorePosted)
			{
				scorePosted = true;
				postScore();
				initGameOver("OUT OF TIME");	
			}
		}
		
		if(inGameOver && flyingGems != null)
		{
			for(var i = 0 ; i < flyingGems.length ; i++)
			{
				flyingGems[i].x -= flyingGems[i].speed;
				if(flyingGems[i].x <= -65)
				{
					flyingGems[i].x = w;
					flyingGems[i].y = (Math.random()*h)-65;
					flyingGems[i].speed = (Math.random()*5)+5;
					flyingGems[i].image = loader.getResult(randomGem());
				}
			}
			update = true;
		}
		
		if(update)
		{
			stage.update();	
			update = false;
		}
	}
	
	//PISTEIDEN POSTAUS
	
	function postScore()
	{
		var highscore = points.text;
		if(highscore > 0)
		{
			$.post("scorePost.php", {score:highscore});
		}
		setTimeout(function(){$("div.scores").load("scoresList.php");}, 2000);
	}
	
	//PELIKUVIEN PÄIVITYS
	
	function updateImageAt(x,y,type)
	{
		if(type === "gemEmpty")
		{
			imageMatrix[x][y].image = null;
		}
		else
		{
			imageMatrix[x][y].image = loader.getResult(type);
		}
		
		update = true;
	}
    	
	//TIMANTTIEN SWAPPI	
		
    function swapGems(firstX, firstY, secondX, secondY)
	{
		if(firstX < 0 || firstX > (MATRIX_SIZE-1) ||
		   firstY < 0 || firstY > (MATRIX_SIZE-1) ||
		   secondX < 0 || secondX > (MATRIX_SIZE-1) ||
		   secondY < 0 || secondY > (MATRIX_SIZE-1))
		{
			return false;
		}
		
		if(firstX != secondX && firstY != secondY)
		{
			return false;
		}
		
		if(Math.abs(firstX - secondX) > 1)
		{
			return false;
		}
		
		if(Math.abs(firstY - secondY) > 1)
		{
			return false;
		}
		
		if(gemMatrix[firstX][firstY].gemType === gemMatrix[secondX][secondY].gemType)
		{
			return false;
		}
		
		var tempType = gemMatrix[firstX][firstY].gemType;
		
		gemMatrix[firstX][firstY].gemType = gemMatrix[secondX][secondY].gemType;
		gemMatrix[secondX][secondY].gemType = tempType;
		
		updateImageAt(firstX,firstY,gemMatrix[firstX][firstY].gemType);
		updateImageAt(secondX,secondY,gemMatrix[secondX][secondY].gemType);
		
		update = true;
		
		return true;
	}
    
	//LUO SATUNNAISEN PELIKENTÄN
	
    function fillMatrix()
    {
        var checkGems = function(i, j)
        {
            var gem = gemMatrix[i][j].gemType;
			if(gem === "gemEmpty")
			{
				return false;
			}
            if(i > 1)
            {
                if(gemMatrix[i-2][j].gemType === gem && gemMatrix[i-1][j].gemType === gem)
				{
					return false;
				}
            }
			if(j > 1)
			{
				if(gemMatrix[i][j-2].gemType === gem && gemMatrix[i][j-1].gemType === gem)
				{
					return false;
				}
			}
			return true;
        };
		
        for(var i = 0 ; i < MATRIX_SIZE ; i++)
        {
            for(var j = 0 ; j < MATRIX_SIZE ; j++)
            {
				do
				{					
					gemMatrix[i][j].gemType = randomGem();
					updateImageAt(i,j,gemMatrix[i][j].gemType);
				}while(!checkGems(i, j))
            }
        }
    }
      
	//PALAUTTAA SATUNNAISEN TIMANTIN  
	  
	function randomGem()
	{
		var x = Math.floor((Math.random() * 7) + 1);
		var y = "gemRed";
		switch(x)
		{
			case 1:
				y = "gemRed";
				break;
			case 2:
				y = "gemBlue";
				break;
			case 3:
				y = "gemGreen";
				break;
			case 4:
				y = "gemOrange";
				break;
			case 5:
				y = "gemWhite";
				break;
			case 6:
				y = "gemPurple";
				break;
			case 7:
				y = "gemYellow";
				break;
			default:
				break;
		}
		return y;
	}
		
	//PELIKENTÄN TARKASTUSTA
	//HAKEE TIMANTTIEN KETJUJA JA LASKEE KUINKA PITKIÄ NE OVAT
	function checkChain(x,y,type,direction,length)
	{		
		switch(direction)
		{
			case "left":
				if(x > 0 && gemMatrix[x-1][y].gemType === type)
				{
					checkChain(x-1,y,type,"left",0);
				}
				else
				{
					checkChain(x,y,type,"right",1);
				}
				break;
			case "up":
				if(y > 0 && gemMatrix[x][y-1].gemType === type)
				{
					checkChain(x,y-1,type,"up",0);
				}
				else
				{
					checkChain(x,y,type,"down",1);
				}
				break;
			case "right":
				if(x < (MATRIX_SIZE-1) && gemMatrix[x+1][y].gemType === type)
				{
					checkChain(x+1,y,type,"right",length+1);
				}
				else if(length > 2)
				{
					for(var i = 0 ; i < length ; i++)
					{
						gemMatrix[x-i][y].deleteThis = true;
					}
				}
				break;
			case "down":
				if(y < (MATRIX_SIZE-1) && gemMatrix[x][y+1].gemType === type)
				{
					checkChain(x,y+1,type,"down",length+1);
				}
				else if(length > 2)
				{
					for(var i = 0 ; i < length ; i++)
					{
						gemMatrix[x][y-i].deleteThis = true;						
					}
				}
				break;
			default:
				if(x > 0)
				{
					checkChain(x,y,type,"left",0);
				}
				else
				{
					checkChain(x,y,type,"right",1);
				}
				if(y > 0)
				{
					checkChain(x,y,type,"up",0);
				}
				else
				{
					checkChain(x,y,type,"down",1);
				}
				break;
		}
		return;
	}
	
	//TARKASTAA ONKO PELIKENTÄSSÄ POISTETTAVIA TIMANTTEJA
	//ANTAA PISTEITÄ POISTETTAVIEN TIMANTTIEN MUKAAN
	//LISÄÄ PÄIVITYSLISTAAN KUVIEN MUUTTAMISEN TYHJÄKSI
	
	function checkDrops()
	{
		for(var i = MATRIX_SIZE-1 ; i >= 0 ; i--)
		{
			for(var j = MATRIX_SIZE-1 ; j >= 0 ; j--)
			{
				if(gemMatrix[i][j].deleteThis)
				{
					gemMatrix[i][j].gemType = "gemEmpty";
					gemMatrix[i][j].deleteThis = false;
					points.text += 5;
					updateQueue.push(new GemVector(i,j,"gemEmpty"));
				}
			}
		}
		
		dropColumns();
	}
	
	//TARKASTAA ONKO PUDOTETTAVIA TIMANTTEJA
	
	function dropColumns()
	{
		for(var i = 0 ; i < MATRIX_SIZE ; i++)
		{
			for(var j = (MATRIX_SIZE-1) ; j >= 0 ; j--)
			{
				while(gemMatrix[i][j].gemType === "gemEmpty")
				{
					dropColumn(i,j);
				}
			}
		}	
	}
	
	//PUDOTTAA TYHJIEN REIKIEN TILALLE UUSIA TIMANTTEJA
	//LISÄÄ PÄIVITYSLISTAAN TIMANTTIEN SIIRROT
	
	function dropColumn(x,y)
	{
		for(var i = y ; i > 0 ; i--)
		{
			gemMatrix[x][i].gemType = gemMatrix[x][i-1].gemType;
			if(gemMatrix[x][i].gemType != "gemEmpty")
			{
				updateQueue.push(new GemVector(x,i,gemMatrix[x][i].gemType));
			}
		}
		gemMatrix[x][0].gemType = randomGem();
		updateQueue.push(new GemVector(x,0,gemMatrix[x][0].gemType));
	}
	
	//SOITTAA ÄÄNEN
	
	function playSound()
	{
		var instance = createjs.Sound.play("blop");
		instance.volume = 0.1;
	}
	
	//KÄY LÄPI PÄIVITYSLISTAN
	//PITÄÄ PÄIVITYSTEN VÄLISSÄ 30 MS TAUON
	//SOITTAA ÄÄNEN JOKA PÄIVITYKSEN VÄLISSÄ
	
	function handleUpdateQueue()
	{
		queueChanged = false;
		if(updateQueue.length > 0)
		{
			var vector = updateQueue.shift();
			playSound();
			updateImageAt(vector.x, vector.y, vector.gemType);
			setTimeout(handleUpdateQueue, 30);			
		}
		return;
	}
   
   
   
   
   
   
   
   
   
   
   
   
   
   