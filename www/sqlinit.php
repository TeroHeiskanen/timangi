<?php

$host = isset($_ENV["DATABASE_HOSTNAME"]) ? $_ENV["DATABASE_HOSTNAME"] : "localhost";

$db = new PDO("mysql:dbname=timangi;host={$host};port=3306;charset=utf8", "timangi", "timangi");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

?>
